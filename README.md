# README #

#Sesim Var!

## Arda Mavi - [ardamavi.com](http://www.ardamavi.com/)

Proje raporu, planı ve özeti Rapor klasöründedir.

### Projede kullanılan kütüphaneler:
OpenCV (cv2)
sklearn
numpy
sqlite3


### Önemli Notlar:
- Programdan en iyi verimi almak için eğitim verisinin düzgün girilmesi çok önemlidir.

- Ağız algılamadaki iyileştirme için yardımlarınızı bekliyorum. 
